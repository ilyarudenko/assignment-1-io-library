section .data

%define SYS_EXIT 60
%define STDOUT 1
%define SYS_WRITE 1
%define LINE_BREAK 0xA

section .text


; Принимает код возврата и завершает текущий процесс
; DONE
exit:
    mov rax, SYS_EXIT
    syscall

; Принимает указатель на нуль-терминированную строку, возвращает её длину
; DONE
string_length:
    xor rax, rax
    .loop:
        cmp byte [rdi+rax], 0
        je .end
        inc rax
        jmp .loop
    .end:
        ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
; DONE
print_string:
    call string_length
    mov  rdx, rax
    mov  rsi, rdi
    mov  rax, SYS_WRITE
    mov  rdi, STDOUT
    syscall
    ret

; Принимает код символа и выводит его в stdout
; DONE
print_char:
    push rdi
    mov rsi, rsp
    mov rax, SYS_WRITE
    mov rdi, STDOUT
    mov rdx, 1
    syscall
    pop rax
    ret

; Переводит строку (выводит символ с кодом 0xA)
; DONE
print_newline:
    mov rdi, LINE_BREAK
    jmp print_char

; Выводит беззнаковое 8-байтовое число в десятичном формате
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
; DONE
print_uint:
    xor rdx, rdx
    xor rsi, rsi
    mov rax, rdi
    mov rcx, LINE_BREAK
    .div:
        div rcx
        push rdx
        xor rdx, rdx
        inc rsi
        cmp rax, 0x0
        je .end_div_loop
        jmp .div
    .end_div_loop:
        pop rdi
        add rdi, '0'
        push rsi
        call print_char
        pop rsi
        dec rsi
        cmp rsi, 0
        je .end_print_uint
        jmp .end_div_loop
    .end_print_uint:
        ret

; Выводит знаковое 8-байтовое число в десятичном формате
; DONE
print_int:
    cmp rdi, 0
    jl .negative
    jmp .positive
    .negative:
        push rdi
        mov rdi, '-'
        call print_char
        pop rdi
        neg rdi
        jmp print_uint
    .positive:
        jmp print_uint

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
; DONE
string_equals:
    xor rcx, rcx

    .loop:
        mov al, [rdi+rcx]
        cmp al, [rsi+rcx]
        jne .neq
        inc rcx
        or al, al
        jnz .loop

        mov rax, 1
        ret
    .neq:
        xor rax, rax
        ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
; DONE
read_char:
    xor rax, rax
    mov rdi, 0
    push rax
    mov rsi, rsp
    mov rdx, 1
    syscall
    pop rax
    cmp al, 0
    je .end_of_stream
    ret
    .end_of_stream:
        xor rax,rax
        ret

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор

read_word:
    push rbx
    push r12
    push r13
    xor rbx, rbx
    mov r12, rdi
    mov r13, rsi
    jmp .loop
    .ws:
        or rbx, rbx
        jnz .retword
    .loop:
        call read_char
        or al, al
        jz .retword
        cmp al, ' '
        je .ws
        cmp al, `\n`
        je .ws
        cmp al, `\t`
        je .ws
        inc rbx
        cmp rbx, r13
        ja .fail
        mov [r12+rbx-1], al
        jmp .loop
    .retword:
        mov byte [r12+rbx], 0
        mov rax, r12
        mov rdx, rbx
        jmp .ret
    .fail:
        xor rax, rax
    .ret:
        pop r13
        pop r12
        pop rbx
        ret
 

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
parse_uint:
    xor rax, rax
    xor rdx, rdx
    .loop:
        movzx r8, byte [rdi+rdx]
        sub r8b, '0'
        jc .break
        cmp r8b, 9
        ja .break
        inc rdx
        add rax, rax
        lea rax, [rax+rax*4]
        add rax, r8
        jmp .loop
    .break:
        ret




; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
; DONE
parse_int:
    cmp byte [rdi], '-'
    jne parse_uint
    inc rdi
    call parse_uint
    neg rax
    inc rdx
    ret 

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
; DONE
string_copy:
    push rdi
	push rsi
	push rdx		    ; Save caller-saved registers
	call string_length
	pop rdx
	pop rsi
	pop rdi			    ; Restore caller-saved registers
	inc rax			    ; Null-terminator space
	cmp rax, rdx		; Compare string and buffer length
	ja .finish
	xor rcx, rcx		; Clear rcx
    .loop:
        mov dl, byte [rdi]
        mov byte [rsi], dl	; Move symbol from string to buffer
        inc rdi
        inc rsi			    ; Increase pointers
        inc rcx		    	; Increase counter
        cmp rcx, rax		; Check buffer status
        jna .loop		    ; If not filled then continue
        dec rax			    ; String length without nul-terminator
        ret
    .finish:
        mov rax, 0		    ; Put 0 in rax
        ret
